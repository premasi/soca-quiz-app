import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import AppleLogo from '../../assets/Apple.svg';

const ContinueAppleButton = ({ text = '' }) => {
  return (
    <TouchableOpacity style={styles.container}>
      <AppleLogo />
      <Text style={styles.title}>{text}</Text>
    </TouchableOpacity>
  );
};

export default ContinueAppleButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 14,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 10,
    marginTop: 23,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginHorizontal: 38,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 14,
    marginStart: 7,
  },
});
