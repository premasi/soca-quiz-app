import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import React from 'react';
import SocaLogo from '../../assets/SocaLogo.svg';
import TextInputLayout from '../views/TextInputLayout';
import ContinueAppleButton from '../views/ContinueAppleButton';
import ContinueGoogleButton from '../views/ContinueGoogleButton';
import ButonView from '../views/ButonView';

const SignUpScreen = ({ navigation }) => {
  return (
    <ScrollView style={styles.scrolContainer}>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={styles.title}>Buat Akun</Text>
        <ContinueAppleButton text="Signup with Apple" />
        <ContinueGoogleButton text="Signup with Google" />
        <Text style={[styles.normalText, { marginTop: 20 }]}>atau</Text>
        <TextInputLayout label="Email" placeholder="Masukkan email kamu" keyboardType="email-address" />
        <TextInputLayout label="No. Hp" placeholder="Masukkan no hp kamu" keyboardType="number-pad" />
        <TextInputLayout label="Password" placeholder="Buat password" secureTextEntry={true} />
        <ButonView text="Daftar" onPress={() => navigation.navigate('signinScreen')} />
        <View style={styles.termNservice}>
          <Text style={styles.normalText}>Dengan melakukan daftar berarti Anda</Text>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.normalText}>menyetujui</Text>
            <TouchableOpacity style={{ marginStart: 4, marginEnd: 4 }}>
              <Text style={styles.textPressable}>syarat dan ketentuan</Text>
            </TouchableOpacity>
            <Text style={styles.normalText}>yang berlaku</Text>
          </View>
        </View>
        <View style={styles.toSignin}>
          <Text style={styles.normalText}>Sudah punya akun?</Text>
          <TouchableOpacity style={{ marginStart: 4 }} onPress={() => navigation.navigate('signinScreen')}>
            <Text style={styles.textPressable}>Masuk disini</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.footer}>Copyright © 2023 Soca.ai All Reserved</Text>
      </View>
    </ScrollView>
  );
};

export default SignUpScreen;

const styles = StyleSheet.create({
  scrolContainer: {
    flex: 1,
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  container: {
    alignItems: 'center',
    marginTop: 107,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 24,
    marginTop: 32,
  },
  normalText: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    fontSize: 12,
    textAlign: 'center',
  },
  termNservice: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 80,
  },
  textPressable: {
    fontFamily: 'avenir-next-demibold',
    color: '#6D75F6',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
  },
  toSignin: {
    flexDirection: 'row',
    marginTop: 56,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 68,
  },
});
