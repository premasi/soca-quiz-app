import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import React from 'react';
import LeftArrow from '../../assets/LeftArrow.svg';
import SocaLogo from '../../assets/SocaLogo.svg';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';
import TextInputLayout from '../views/TextInputLayout';

const NewPasswordScreen = ({ navigation }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TouchableHighlight onPress={() => navigation.navigate('forgotPasswordScreen')}>
          <View style={styles.back}>
            <LeftArrow />
            <Text style={[textStyles.semiBoldText, { fontSize: 20, marginStart: 4 }]}>Kembali</Text>
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={[textStyles.semiBoldText, { fontSize: 24, marginTop: 32 }]}>Buat Password Baru</Text>
        <Text style={[textStyles.normalText, { fontSize: 14, marginTop: 8, marginBottom: 24, textAlign: 'center' }]}>Silakan untuk membuat password baru kamu</Text>
        <TextInputLayout label="Password Baru" placeholder="Masukkan password baru kamu" secureTextEntry={true} />
        <TextInputLayout label="Konfirmasi Password" placeholder="Konfirmasi password" secureTextEntry={true} />
        <ButonView text="Simpan" onPress={() => navigation.navigate('signinScreen')} />
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View>
    </View>
  );
};

export default NewPasswordScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footer: {
    backgroundColor: '#000',
    marginBottom: 26,
  },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
});
