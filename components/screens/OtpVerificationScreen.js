import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity, TouchableHighlight } from 'react-native';
import React, { useState, useRef } from 'react';
import LeftArrow from '../../assets/LeftArrow.svg';
import SocaLogo from '../../assets/SocaLogo.svg';
import OtpInputLayout from '../views/OtpInputLayout';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';

const OtpVerificationScreen = ({ navigation }) => {
  const pin1Ref = useRef(null);
  const pin2Ref = useRef(null);
  const pin3Ref = useRef(null);
  const pin4Ref = useRef(null);
  const pin5Ref = useRef(null);
  const pin6Ref = useRef(null);

  const [pin1, setPin1] = useState('');
  const [pin2, setPin2] = useState('');
  const [pin3, setPin3] = useState('');
  const [pin4, setPin4] = useState('');
  const [pin5, setPin5] = useState('');
  const [pin6, setPin6] = useState('');
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TouchableHighlight onPress={() => navigation.navigate('forgotPasswordScreen')}>
          <View style={styles.back}>
            <LeftArrow />
            <Text style={[textStyles.semiBoldText, { fontSize: 20, marginStart: 4 }]}>Kembali</Text>
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={[textStyles.semiBoldText, { fontSize: 24, marginTop: 32 }]}>Lupa Password</Text>
        <Text style={[textStyles.normalText, { fontSize: 14, marginTop: 8, marginBottom: 77, marginHorizontal: 75, textAlign: 'center' }]}>Masukkan kode verifikasi yang dikirimkan ke email kamu</Text>
        <View style={{ flexDirection: 'row' }}>
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin1Ref}
            onChange={(pin1) => {
              setPin1(pin1);
              if (pin1 !== '') {
                pin2Ref.current.focus();
              }
            }}
          />
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin2Ref}
            onChange={(pin2) => {
              setPin2(pin2);
              if (pin2 !== '') {
                pin3Ref.current.focus();
              }
            }}
          />
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin3Ref}
            onChange={(pin3) => {
              setPin3(pin3);
              if (pin3 !== '') {
                pin4Ref.current.focus();
              }
            }}
          />
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin4Ref}
            onChange={(pin4) => {
              setPin4(pin4);
              if (pin4 !== '') {
                pin5Ref.current.focus();
              }
            }}
          />
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin5Ref}
            onChange={(pin5) => {
              setPin5(pin5);
              if (pin5 !== '') {
                pin6Ref.current.focus();
              }
            }}
          />
          <OtpInputLayout
            keyboardType="number-pad"
            ref={pin6Ref}
            onChange={(pin6) => {
              setPin6(pin6);
            }}
          />
        </View>
        <ButonView text="Kirim" onPress={() => navigation.navigate('newPasswordScreen')} />
        <View style={styles.resendCode}>
          <Text style={[textStyles.normalText, { fontSize: 12 }]}>
            Belum menerima kode?{' '}
            <TouchableOpacity style={{ marginTop: -3 }}>
              <Text style={styles.textPressable}>Kirim ulang</Text>
            </TouchableOpacity>
          </Text>
        </View>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View>
    </View>
  );
};

export default OtpVerificationScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footer: {
    backgroundColor: '#000',
    marginBottom: 26,
  },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
  textPressable: {
    fontFamily: 'avenir-next-demibold',
    color: '#6D75F6',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
  },
  resendCode: {
    flexDirection: 'row',
    marginTop: 44,
  },
});
